const { authJwt } = require("../middleware");
const controller = require("../controllers/user.controller");
const ssfusercontroller = require("../controllers/ssfuser.controller");
const bookcontroller = require("../controllers/book.controller");
const recordcontroller = require("../controllers/record.controller");
const paymentcontroller = require("../controllers/payment.controller");
const { verifySignUp } = require("../middleware");
const ucontroller = require("../controllers/auth.controller");

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.get("/api/test/all", controller.allAccess);

  app.get(
    "/api/test/student/:name",
    [authJwt.verifyToken],
    controller.studentBoard
  );

  app.get(
    "/api/test/faculty",
    [authJwt.verifyToken],
    controller.facultyBoard
  );

  app.get(
    "/api/test/librarian/ssfusers/:name",
    [authJwt.verifyToken, authJwt.isLibrarian],
    ssfusercontroller.findAll
  );
  app.get(
    "/api/test/librarian/ssfusers",
    [authJwt.verifyToken, authJwt.isLibrarian],
    ssfusercontroller.filter
  );
  app.get(
    "/api/test/librarian/ssfusers",
    [authJwt.verifyToken, authJwt.isLibrarian],
    ssfusercontroller.sort
  );

  app.post(
    "/api/test/librarian/books",
    [authJwt.verifyToken, authJwt.isLibrarian],
    bookcontroller.create
  );
  app.get(
    "/api/test/librarian/books",
    [authJwt.verifyToken, authJwt.isLibrarian],
    bookcontroller.findAll
    );
    app.get(
      "/api/test/librarian/books/:id",
      [authJwt.verifyToken, authJwt.isLibrarian],
      bookcontroller.findOne
    );
    app.put(
      "/api/test/librarian/books/:id",
      [authJwt.verifyToken, authJwt.isLibrarian],
      bookcontroller.update
    );
    app.delete(
      "/api/test/librarian/books/:id",
      [authJwt.verifyToken, authJwt.isLibrarian],
      bookcontroller.delete
    );
    app.delete(
      "/api/test/librarian/books",
      [authJwt.verifyToken, authJwt.isLibrarian],
      bookcontroller.deleteAll
    );

    app.post(
      "/api/test/librarian/records",
      [authJwt.verifyToken, authJwt.isLibrarian,recordcontroller.checkbook],
      recordcontroller.create
    );
    app.get(
      "/api/test/librarian/records",
      [authJwt.verifyToken, authJwt.isLibrarian],
      recordcontroller.findAll
      );
      app.get(
        "/api/test/librarian/records/:id",
        [authJwt.verifyToken, authJwt.isLibrarian],
        recordcontroller.findOne
      );
      app.put(
        "/api/test/librarian/records/:id",
        [authJwt.verifyToken, authJwt.isLibrarian],
        recordcontroller.update
      );
      app.delete(
        "/api/test/librarian/records/:id",
        [authJwt.verifyToken, authJwt.isLibrarian],
        recordcontroller.delete
      );
      app.delete(
        "/api/test/librarian/records",
        [authJwt.verifyToken, authJwt.isLibrarian],
        recordcontroller.deleteAll
      );

      
    app.post(
      "/api/test/librarian/payments",
      [authJwt.verifyToken, authJwt.isLibrarian],
      paymentcontroller.create
    );
    app.get(
      "/api/test/librarian/payments",
      [authJwt.verifyToken, authJwt.isLibrarian],
      paymentcontroller.findAll
      );
      app.get(
        "/api/test/librarian/payments/:id",
        [authJwt.verifyToken, authJwt.isLibrarian],
        paymentcontroller.findOne
      );
      app.put(
        "/api/test/librarian/payments/:id",
        [authJwt.verifyToken, authJwt.isLibrarian],
        paymentcontroller.update
      );
      app.delete(
        "/api/test/librarian/payments/:id",
        [authJwt.verifyToken, authJwt.isLibrarian],
        paymentcontroller.delete
      );
      app.delete(
        "/api/test/librarian/payments",
        [authJwt.verifyToken, authJwt.isLibrarian],
        paymentcontroller.deleteAll
      );

       
      app.post(
        "/api/test/librarian/users",
        [
          verifySignUp.checkDuplicateUsernameOrEmail,
          verifySignUp.checkRolesExisted
        ],
        ucontroller.signup
      );
    app.get(
      "/api/test/librarian/users",
      [authJwt.verifyToken, authJwt.isLibrarian],
      controller.findAll
      );
      app.get(
        "/api/test/librarian/users/:id",
        [authJwt.verifyToken, authJwt.isLibrarian],
        controller.findOne
      );
      app.put(
        "/api/test/librarian/users/:id",
        [authJwt.verifyToken, authJwt.isLibrarian],
        controller.update
      );
      app.delete(
        "/api/test/librarian/users/:id",
        [authJwt.verifyToken, authJwt.isLibrarian],
        controller.delete
      );
      app.delete(
        "/api/test/librarian/users",
        [authJwt.verifyToken, authJwt.isLibrarian],
        controller.deleteAll
      );
};
