module.exports = app => {
  const books = require("../controllers/book.controller.js");
  const ssfbook = require("../controllers/ssfbook.controller.js");
  const records = require("../controllers/record.controller.js");
 // const payments = require("../controllers/payment.controller.js");
  const statistics=require("../controllers/statistics.controller.js")
  var router = require("express").Router();

  router.get("/oldestbook",statistics.oldestbook);          // Find oldest book
 /* router.get("/newestbook",statistics.newestbook);          // Find newest book
  router.get("/heighstlentbook",statistics.heighstlentbook);      //Find heighst lent book
  router.get("/totallentbook",statistics.totallentbook);          //Find total lent book
  router.get("/totalbooks",statistics.totalbooks);               // Find total books
  //router.get("/mostactiveuser",statistics.mostactiveuser);        //Find most active user
 // router.get("/totaluser",statistics.totaluser);                  //Find total user
  //router.get("/oldestbook",statistics.oldestbook);   
  router.get("/sort", book.sort);
  */
  // Insert an item 
  //router.post("/books", books.create);             //Insert a book
  //router.post("/records", records.create);          //Insert a record
 // router.post("/payments", payments.create);        //Insert a payment
  
  // Fatch a list of all
  //router.get("/books", books.findAll);
  router.get("/book/:name", ssfbook.findAll);              //Fatch all book by name
  router.get("/books", ssfbook.filter);
  router.get("/book", ssfbook.sort);
  //router.get("/records", records.findAll);            //Fatch all record
 // router.get("/payments", payments.findAll);           //Fatch all payment

/*
  // Fatch an item with id
  router.get("/books/:id", books.findOne);                  //Fatch a book by id
  router.get("/records/:id", records.findOne);                  //Fatch a record by id
 // router.get("/payments/:id", payments.findOne);                  //Fatch a payment by id

  // Update an item with id
  router.put("/books/:id", books.update);                   //Update a book by id
  router.put("/records/:id", records.update);                   //Update a record by id
  //router.put("/payments/:id", payments.update);                   //Update a payment by id

  // Delete an item with id
  router.delete("/books/:id", books.delete);                //Delete a book by id
  router.delete("/records/:id", records.delete);                //Delete a record by id
 // router.delete("/payments/:id", payments.delete);                //Delete a payment by id

  // Delete all item
  router.delete("/books", books.deleteAll);                //Delete all books
  router.delete("/records", records.deleteAll);                //Delete all records
  //router.delete("/payments", payments.deleteAll);                //Delete all payments
*/
  app.use("/api", router);
};
