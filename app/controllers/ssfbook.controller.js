const db = require("../models");
const Book = db.books;
const Record =db.records;
const Op = db.Sequelize.Op;
const Sequelize = require("sequelize");


// Retrieve all Books by subject from the database.
exports.findAll = (req, res) => {
    const name = req.params.name;
    var condition = name ? { name: { [Op.iLike]: `%${name}%` } } : null;
  
    Book.findAll({ where: condition })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving books."
        });
      });
  };
  

  exports.filter = (req, res) => {
    const subject = req.query.subject;
    var condition = subject ? { subject: { [Op.iLike]: `%${subject}%` } } : null;
  
    Book.findAll({ where: condition })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving books."
        });
      });
  };

  exports.sort = (req, res) => {
   
    Book.findAll({ where: {},
      
    attributes: ['name',[Sequelize.fn('count', Sequelize.col('id')),'Copies_available']],
    group:["name"],
    order: [
      ['name','ASC']
     // ['Available copies', 'DESC']
  ]
    })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving books."
        });
      });
  };