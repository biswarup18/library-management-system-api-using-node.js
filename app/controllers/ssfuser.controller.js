const db = require("../models");
const User =db.user;
const Op = db.Sequelize.Op;
const Sequelize = require("sequelize");


// Retrieve all Books by subject from the database.
exports.findAll = (req, res) => {
    const name = req.params.name;
    var condition = name ? { name: { [Op.iLike]: `%${name}%` } } : null;
  
    User.findAll({ where: condition })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving Users."
        });
      });
  };
  

  exports.filter = (req, res) => {
    const catagory = req.query.catagory;
    var condition = catagory ? { catagory: { [Op.iLike]: `%${catagory}%` } } : null;
  
    User.findAll({ where: condition })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving Users."
        });
      });
  };

  exports.sort = (req, res) => {
   
    User.findAll({ where: {},
    order: [
      ['username','ASC']
      ['registration_date','DESC']
  ]
    })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving Users."
        });
      });
  };