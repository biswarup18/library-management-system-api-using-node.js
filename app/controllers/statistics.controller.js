const db = require("../models");
const Book = db.books;
const User = db.user;
const Record = db.records;
const Op = db.Sequelize.Op;
const Sequelize = require("sequelize");


//Find Oldest Book 
exports.oldestbook = (req, res) => {    
     Book.findOne({ where: {}, 
      order: [
        ['published_date', 'ASC']
    ],   
      attributes: ['id', 'ISBN', 'name', 'subject', 'author','published_date']   
     })     
       .then(data => {
         console.log('Oldest Book');
         res.send(data);
       })
       .catch(err => {
         res.status(500).send({
           message:
             err.message || "Some error occurred while retrieving books."
         });
       }),
   //};


   //Find Heighst lent book
   //exports.heighstlentbook = (req, res) => {    
    Record.findOne({ where: {}, 
     attributes: [[Sequelize.fn('max', Sequelize.col('booked')),'Heighst lent Book']]
    })     
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving books."
        });
      });
  };


  //Find newest book
  exports.newestbook = (req, res) => {    
    Book.findOne({ where: {}, 
     order: [
       ['published_date', 'DESC']
   ],   
     attributes: ['id', 'ISBN', 'name', 'subject', 'author','published_date']   
    })     
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving books."
        });
      });
  };


  //Total lent book
  exports.totallentbook = (req, res) => {    
    Record.findAll({ where: {},   
    attributes: [[Sequelize.fn('count', Sequelize.col('booked')),'Total lent book']]
    })     
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving books."
        });
      });
  };


  //Total books
  exports.totalbooks = (req, res) => {    
    Book.findOne({ where: {}, 
      attributes: [[Sequelize.fn('count', Sequelize.col('id')),'Total books']]
    })     
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving books."
        });
      });
  };



  exports.mostactiveuser = (req, res) => {    
    Record.findOne({ where: {}, 
      
    attributes: [[Sequelize.fn('max', Sequelize.col('user_id')),'most active user']]
    })     
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving books."
        });
      });
  };
  


  exports.totaluser = (req, res) => {    
    User.findAll({ where: {}, 
    attributes: [[Sequelize.fn('count', Sequelize.col('id')),'Total users']]
    })     
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving books."
        });
      });
  };
  