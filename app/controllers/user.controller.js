const db = require("../models");
const Book = db.books;
const User = db.user;

exports.allAccess = (req, res) => {
    // const name = req.query.name;
    // var condition = name ? { name: { [Op.iLike]: `%${name}%` } } : null;
   
     Book.findAll({ where: {},
       order: [
         ['id', 'DESC'],
         ['name', 'ASC'],
     ],
     attributes: ['id', 'ISBN', 'name', 'subject', 'author','published_date']
     })
       .then(data => {
         res.send(data);
       })
       .catch(err => {
         res.status(500).send({
           message:
             err.message || "Some error occurred while retrieving books."
         });
       });
   };
 


exports.studentBoard = (req, res) => {
  const name = req.params.name;
    var condition = name ? { name: { [Op.iLike]: `%${name}%` } } : null;
  
    Book.findAll({ where: condition })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving books."
        });
      });
  };
 // res.status(200).send("Student Content.");


exports.librarianBoard = (req, res) => {
  res.status(200).send("Librarian Content.");
};

exports.facultyBoard = (req, res) => {
  res.status(200).send("Faculty Content.");
};





exports.findAll = (req, res) => {
  // const name = req.query.name;
  // var condition = name ? { name: { [Op.iLike]: `%${name}%` } } : null;
 
   User.findAll({ where: {},
     order: [
       ['id', 'DESC'],
       ['username', 'ASC'],
   ],
   attributes: ['id', 'username', 'email','password','registration_date']
   })
     .then(data => {
       res.send(data);
     })
     .catch(err => {
       res.status(500).send({
         message:
           err.message || "Some error occurred while retrieving users."
       });
     });
 };
 
 
 // Find a single Book with an id
 exports.findOne = (req, res) => {
   const id = req.params.id;
 
   User.findByPk(id)
     .then(data => {
       res.send(data);
     })
     .catch(err => {
       res.status(500).send({
         message: "Error retrieving User with id=" + id
       });
     });
 };
 
 // Update a Book by the id in the request
 exports.update = (req, res) => {
   const id = req.params.id;
 
   User.update(req.body, {
     where: { id: id }
   })
     .then(num => {
       if (num == 1) {
         res.send({
           message: "User was updated successfully."
         });
       } else {
         res.send({
           message: `Cannot update User with id=${id}. Maybe User was not found or req.body is empty!`
         });
       }
     })
     .catch(err => {
       res.status(500).send({
         message: "Error updating User with id=" + id
       });
     });
 };
 
 // Delete a Book with the specified id in the request
 exports.delete = (req, res) => {
   const id = req.params.id;
 
   User.destroy({
     where: { id: id }
   })
     .then(num => {
       if (num == 1) {
         res.send({
           message: "User was deleted successfully!"
         });
       } else {
         res.send({
           message: `Cannot delete User with id=${id}. Maybe User was not found!`
         });
       }
     })
     .catch(err => {
       res.status(500).send({
         message: "Could not delete User with id=" + id
       });
     });
 };
 
 // Delete all Books from the database.
 exports.deleteAll = (req, res) => {
   User.destroy({
     where: {},
     truncate: false
   })
     .then(nums => {
       res.send({ message: `${nums} Users were deleted successfully!` });
     })
     .catch(err => {
       res.status(500).send({
         message:
           err.message || "Some error occurred while removing all Users."
       });
     });
 };
 
