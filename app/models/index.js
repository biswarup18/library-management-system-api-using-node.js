const config = require("../config/db.config.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(
  config.DB,
  config.USER,
  config.PASSWORD,
  {
    host: config.HOST,
    dialect: config.dialect,
    operatorsAliases: false,

    pool: {
      max: config.pool.max,
      min: config.pool.min,
      acquire: config.pool.acquire,
      idle: config.pool.idle
    }
  }
);
//const db = {Role: sequelize.import("./role.models")};
const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.user = require("../models/user.model.js")(sequelize, Sequelize);
db.role = require("../models/role.model.js")(sequelize, Sequelize);
db.books = require("../models/book.model.js")(sequelize, Sequelize);
db.payments = require("../models/payment.model.js")(sequelize, Sequelize);
db.records = require("../models/record.model.js")(sequelize, Sequelize);

db.books.hasMany(db.records);
db.user.hasMany(db.records);
db.records.hasOne(db.payments);

db.role.belongsToMany(db.user, {
  through: "user_roles",
  foreignKey: "roleId",
  otherKey: "userId"
});
db.user.belongsToMany(db.role, {
  through: "user_roles",
  foreignKey: "userId",
  otherKey: "roleId"
});

db.ROLES = ["student", "librarian", "faculty"];

module.exports = db;
