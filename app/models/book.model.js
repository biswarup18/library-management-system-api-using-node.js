module.exports = (sequelize, Sequelize) => {
  const Book = sequelize.define("books", {
    ISBN: {
      type: Sequelize.INTEGER,
      unique: true,
      allowNull: false
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false
    },
    subject: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    author: {
      type: Sequelize.STRING,
      allowNull: false
    },
    published_date: {
      type: Sequelize.DATE,
      allowNull: false,
    } 
});
  return Book;
};
