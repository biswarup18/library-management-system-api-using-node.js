//const { books } = require("../app/models");
//const { user } = require("../app/models");

module.exports = (sequelize, Sequelize) => {
  
    const Record = sequelize.define("records", {
      bookId: Sequelize.INTEGER,
      userId: Sequelize.INTEGER,
        issue_date: {
            type: Sequelize.DATE,
            allowNull: false,
          },
      submit_date: {
            type: Sequelize.DATE,
            allowNull: false,
          },
          booked: {
           type: Sequelize.INTEGER,
          allowNull: false,
          },
           
        },
        {
           book_id:{
            type: Sequelize.INTEGER,
            references: {
              model:"books",
              key: "id",
                        }
        }
      },
      {
             user_id:{
             type: Sequelize.INTEGER,
             references: {
               model:"users",
               key: "id",
                         }
          }        
        }
        );        
    return Record;
  };
  