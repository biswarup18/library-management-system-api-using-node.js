//const { records } = require("../app/models");

module.exports = (sequelize, Sequelize) => {
  
    const Payment = sequelize.define("payments", {
        fine: Sequelize.INTEGER,
           recordId: Sequelize.INTEGER,
        },
        {
           record_id:{
            type: Sequelize.INTEGER,
            references: {
              model:"records",
              key: "id",
                        }
                   }
        });        
    return Payment;
  };